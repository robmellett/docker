#!/bin/bash

CURRENT_VERSION="8.0"

# Search and replace PHP Versions in all files
grep -rli "$CURRENT_VERSION" * | xargs -i@ sed -i "s/$CURRENT_VERSION/$PHP_VERSION/g" @